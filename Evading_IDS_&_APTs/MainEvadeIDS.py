import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense , Dropout
from keras.optimizers import RMSprop , adam
#from cleverhans.attacks import fgsm , jsma
from cleverhans.utils_tf import model_train , model_eval , batch_eval
from cleverhans.attacks_tf import jacobian_graph
from cleverhans.utils import other_classes
import tensorflow as tf
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score , roc_curve , auc , f1_score
from sklearn.preprocessing import LabelEncoder , MinMaxScaler
import matplotlib.pyplot as plt


from tensorflow.python.platform import flags

#The next step is pre-processing the data:
names = ['duration', 'protocol', 'service ', 'flag', 'src_bytes', 'dst_bytes', 'land',
'wrong_fragment ','urgent ', 'hot', 'num_failed_logins ', 'logged_in ', 'num_compromised ', 'root_shell ', 'su_attempted ','num_root ', 'num_file_creations ', 'num_shells ', 'num_access_files ', 'num_outbound_cmds ','is_host_login ', 'is_guest_login ', 'count', 'srv_count ', 'serror_rate', 'srv_serror_rate ','rerror_rate ', 'srv_rerror_rate ', 'same_srv_rate ', 'diff_srv_rate', 'srv_diff_host_rate ','dst_host_count ', 'dst_host_srv_count ', 'dst_host_same_srv_rate ', 'dst_host_diff_srv_rate ','dst_host_same_src_port_rate ', 'dst_host_srv_diff_host_rate ', 'dst_host_serror_rate ','dst_host_srv_serror_rate ','dst_host_rerror_rate ', 'dst_host_srv_rerror_rate ','attack_type ', 'other ']

df = pd.read_csv('KDDTrain+.txt', names=names , header=None)
dft = pd.read_csv('KDDTest+.txt', names=names , header=None)



FLAGS = flags.FLAGS
flags.DEFINE_integer('batch_size', 128, 'Size of training batches ')
flags.DEFINE_float('learning_rate', 0.1, 'Learning rate for training ')
flags.DEFINE_integer('nb_classes', 5, 'Number of classification classes ')
flags.DEFINE_integer('source_samples', 10, 'Nb of test set examples to attack ')
full = pd.concat([df,dft])
assert full.shape[0] == df.shape[0] + dft.shape[0]
print("Initial test and training data shapes:", df.shape , dft.shape)


#We will then load the data with pandas:
TrainingData = pd.read_csv('KDDTrain+.txt', names=names , header=None)
TestingData = pd.read_csv('KDDTest+.txt', names=names , header=None)

#Then, concatenate the training and testing sets:
All = pd.concat ([TrainingData, TestingData])
assert full.shape[0] == TrainingData.shape[0] + TestingData.shape[0]

#Select the data and identify the features:
#All['label'] = full['attack_type']

#To identify DoS attacks, use the following:
All.loc[All.label == 'neptune ', 'label'] = 'dos'
All.loc[All.label == 'back', 'label '] = 'dos'
All.loc[All.label == 'land', 'label '] = 'dos'
All.loc[All.label == 'pod', 'label'] = 'dos'
All.loc[All.label == 'smurf ', 'label'] = 'dos'
All.loc[All.label == 'teardrop ', 'label '] = 'dos'
All.loc[All.label == 'mailbomb ', 'label '] = 'dos'
All.loc[All.label == 'processtable ', 'label'] = 'dos'
All.loc[All.label == 'udpstorm ', 'label '] = 'dos'
All.loc[All.label == 'apache2 ', 'label'] = 'dos'
All.loc[All.label == 'worm', 'label '] = 'dos'

#Identify other attacks (User-to-Root (U2R), Remote-to -Local (R2L), and Probe) with the same technique.
#To generate one-hot encoding, use the following:
full = pd.get_dummies(All , drop_first=False)

#Identify the training and testing sets again:
features = list(full.columns [:-5])
y_train = np.array(full[0:TrainingData.shape[0]][[ 'label_normal ', 'label_dos ', 'label_probe', 'label_r2l ', 'label_u2r ']])
X_train = full[0:TrainingData.shape[0]][ features]
y_test = np.array(full[TrainingData.shape[0]:][[ 'label_normal ', 'label_dos ', 'label_probe ', 'label_r2l ', 'label_u2r ']])
X_test = full[TrainingData.shape[0]:][features]

#To scale the data, use the following command:
scaler = MinMaxScaler().fit(X_train)
#An example of scale X_train is as follows:
X_train_scaled = np.array(scaler.transform(X_train))


#Let's suppose that we are going to attack a logistic regression model; we need to process data to train that model and generate label encoding:
labels = All.label.unique()
En = LabelEncoder()
En.fit(labels)
y_All = En.transform(All.label)
y_train_l = y_All[0:TrainingData.shape[0]]
y_test_l = y_All[TrainingData.shape[0]:]


#We have now completed the pre-processing phase.
#For the Jacobian-Based Saliency Map attack, we are going to use the following Python implementation:

results = np.zeros((FLAGS.nb_classes , source_samples), dtype='i')
perturbations = np.zeros((FLAGS.nb_classes , source_samples), dtype='f')
grads = jacobian_graph(predictions , x, FLAGS.nb_classes)
X_adv = np.zeros(( source_samples , X_test_scaled.shape [1]))
for sample_ind in range(0, source_samples):
    current_class = int(np.argmax(y_test[sample_ind ]))
    for target in [0]:
       if current_class == 0:
                Break
adv_x , res , percent_perturb = jsma(sess , x, predictions , grads,X_test_scaled[sample_ind: (sample_ind+1)],target , theta=1, gamma =0.1,increase=True , back='tf',clip_min=0, clip_max =1)
X_adv[sample_ind] = adv_x
results[target , sample_ind] = res
perturbations[target , sample_ind] = percent_perturb


#To build a MultiLayer Perceptron network, use the following code snippet:
def mlp_model ():

#Generate a MultiLayer Perceptron model
    model = Sequential ()
    model.add(Dense (256, activation='relu', input_shape =( X_train_scaled.shape [1],)))
    model.add(Dropout (0.4))
    model.add(Dense (256, activation='relu'))
    model.add(Dropout (0.4))
    model.add(Dense(FLAGS.nb_classes , activation='softmax '))
    model.compile(loss='categorical_crossentropy ',optimizer='adam',metrics =['accuracy '])
    model.summary ()
    return model