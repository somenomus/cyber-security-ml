import BotnetDataPrepare
import LoadData
import pickle

file = open('flowdata.pickle', 'rb')
data = pickle.load(file)

#selecting the data sections
Xdata = data[0]
Ydata = data[1]
XdataT = data[2]
YdataT = data[3]

#..............................................1st algo decision tree classifier
import sklearn
from sklearn.linear_model import *
from sklearn.tree import *
from sklearn.naive_bayes import *
from sklearn.neighbors import *

BotnetDataPrepare.Prepare(Xdata, Ydata, XdataT, YdataT)

#Now will train our model
clf = sklearn.tree.DecisionTreeClassifier()
clf.fit(Xdata, Ydata)
Prediction = clf.predict(XdataT)
Score = clf.score(XdataT, YdataT)
print("score: " , Score *100)


#..............................................2nd, Logistic regression
clf = LogisticRegression(C=1000)
clf.fit(Xdata, Ydata)
Prediction = clf.predict(XdataT)
Score = clf.score(XdataT, YdataT)
print("score: " , Score *100)




#..............................................3nd, Gaussian NB
clf = GaussianNB()
clf.fit(Xdata, Ydata)
Prediction = clf.predict(XdataT)
Score = clf.score(XdataT, YdataT)
print("score: " , Score *100)


#..............................................4nd, KNN
clf = KNeighborsClassifier()
clf.fit(Xdata, Ydata)
Prediction = clf.predict(XdataT)
Score = clf.score(XdataT, YdataT)
print("score: " , Score *100)






